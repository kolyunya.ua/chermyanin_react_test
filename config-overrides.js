const { override, fixBabelImports, addLessLoader } = require('customize-cra');

module.exports = override(
	fixBabelImports('import', {
		libraryName: 'antd',
		libraryDirectory: 'es',
		style: true,
	}),
	addLessLoader({
		javascriptEnabled: true,
		modifyVars: {
			// Colors
			'@primary-color': '#00b67b',

			// Layout
			'@layout-header-background': '#fff',
			'@layout-header-padding': '0', // '0 16px',
			'@layout-footer-padding': '0.875rem 1.125rem', // '14px 18px',

			// Menu
			'@menu-dark-bg': '#022b3d',
			'@menu-collapsed-width': '3.75rem', // '60px',
			'@menu-dark-submenu-bg': '#000',
		},
	}),
);
