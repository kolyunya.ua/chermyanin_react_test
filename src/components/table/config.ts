export const ETableBaseConfig = {
	className: 'table',
	scroll: {x: 1400},
	size: 'middle',
	bordered: true
} as const;

export const ETablePageSizeOptions = ['10', '20', '40', '90'] as string[];
