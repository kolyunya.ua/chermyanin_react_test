import { compose } from 'redux';
import { connect } from 'react-redux';
import { IStoreState } from 'store/types';
import { IMapStateToProps, IMapDispatchToProps } from './types';
import { View } from './view';
import { getContactsFilters, getContactsList } from 'store/contacts/selectors';
import { hasPreloader } from 'store/preloader/selectors';
import { fetchContactsCollection } from 'store/contacts/actions'

const mapStateToProps = (state: IStoreState): IMapStateToProps => {
	return {
		contactsList: getContactsList(state),
		contactsListPending: hasPreloader(state, fetchContactsCollection.type),
		filtersData: getContactsFilters(state)
	};
};

const mapDispatchToProps: IMapDispatchToProps = {
	fetchContactsCollection,
};

const TableContacts = compose(
	connect(mapStateToProps, mapDispatchToProps)
)(View);

export { TableContacts };
