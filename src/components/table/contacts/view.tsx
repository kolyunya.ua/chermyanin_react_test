import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Table, Avatar, Tag } from 'antd';
import { Truncate } from 'components';
import { routes } from 'routes';
import { makeClasses } from 'utils';
import { TComponentProps } from './types';
import { TABLE_NAME } from './config';
import { ETableBaseConfig, ETablePageSizeOptions } from 'components/table/config';

const View: React.FC<TComponentProps> = (props) => {
	const {
		contactsList,
		contactsListPending,
		filtersData,
		fetchContactsCollection
	} = props;
	const { info = {}, results = [] } = contactsList;

	const currentUser = routes['contacts-view'].link;
	const tableClassNames = makeClasses('table', [TABLE_NAME]);


	useEffect(() => {
		fetchContactsCollection({
			params: {
				results: 100
			}
		});
	}, [fetchContactsCollection])

	const onTableChange = (pagination: any, filters: any, sorter: any) => {
		const data = {
			...filtersData,
			...filters
		};
		data.page = pagination.current;
		data.per_page = pagination.per_page;
		data.seed = "123";
		if (pagination) {
			data.page = pagination.current;
			data.per_page = pagination.pageSize;
		}
		if (sorter) {
			data.order_by = sorter.field;
			data.order_type = sorter.order === 'ascend' ? 'asc' : 'desc';
		}
		fetchContactsCollection({ params: data })
	};

	return (
		<Table
			{...ETableBaseConfig}
			className={tableClassNames}
			loading={contactsListPending}
			rowKey={(record) => String(record.login.uuid)}
			bordered={false}
			dataSource={results}
			pagination={{
				pageSize: filtersData.per_page,
				total: info.results,
				current: info.page,
				showSizeChanger: true,
				pageSizeOptions: ETablePageSizeOptions
			}}
			columns={[
				{
					title: 'Avatar',
					dataIndex: 'picture',
					width: '150px',
					align: 'center',
					render: (picture) => {
						return (
							<Avatar size={40} src={picture.thumbnail} />
						)
					},
				},
				{
					title: 'Full name',
					dataIndex: 'full_name',
					sorter: true,
					render: (full_name: string, _: any, index: number) => {
						return (
							<Link to={currentUser(index + 1)}>{full_name}</Link>
						)
					},
				},
				{
					title: 'Birthday',
					dataIndex: 'date',
					render: (_: any, record: any) => {
						const { date, age } = record;
						return (
							<>
								<div>{date}</div>
								<div>{age} years</div>
							</>
						)
					},
				},
				{
					title: 'Email',
					dataIndex: 'email',
					render: (email: string) => {
						return (
							<Truncate type={'email'} copy={email}>{email}</Truncate>
						)
					},
				},
				{
					title: 'Phone',
					dataIndex: 'phone',
					render: (phone: string) => {
						return (
							<Truncate.Phone phone={{ number: phone }} />
						)
					},
				},
				{
					title: 'Location',
					dataIndex: 'phone',
					render: (_: any, record: any) => {
						const { country, address } = record;
						return (
							<Truncate type={'text'} copy={`[${country}] ${address}`}>
								<strong>/{country}/</strong>
								<div>{address}</div>
							</Truncate>
						)
					},
				},
				{
					title: 'Nationality',
					dataIndex: 'nat',
					render: (nat: string) => {
						return (
							<Tag color={'volcano'}>{nat}</Tag>
						)
					},
				},
			]}
			onChange={onTableChange}
		/>
	)
};

export { View };
