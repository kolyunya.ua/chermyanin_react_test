import { IAction } from 'store/types';
import { IContactsCollectionList } from 'store/contacts/types';

/**
 * IOwnProps
 * */
export interface IOwnProps {}



/**
 * TConnectProps
 * */
export interface IMapStateToProps {
	contactsList: IContactsCollectionList;
	contactsListPending: boolean;
	filtersData: any;
}
export interface IMapDispatchToProps {
	fetchContactsCollection(payload: any): IAction<any>;
}
export type TConnectProps = IMapStateToProps & IMapDispatchToProps;



/**
 * TComponentProps
 * */
export type TComponentProps = IOwnProps & TConnectProps;
