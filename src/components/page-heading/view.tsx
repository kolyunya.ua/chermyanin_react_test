import React from 'react';
import { Typography, Button, Col, Row } from 'antd';
import { DesktopTooltip } from 'components';
import { TComponentProps } from './types';

const View: React.FC<TComponentProps> = (props) => {
	const { title, actions = [] } = props;

	return (
		<div className={'page-heading'}>
			<Row gutter={[12, 16]} justify={'space-between'} align={'middle'}>
				<Col>
					<Typography.Title className={'_m-none'} level={2}>{title}</Typography.Title>
				</Col>
				<Col>
					{actions.length ? (
						<Row gutter={[12, 8]} align={'middle'}>
							{actions.map((action, idx) => {
								const { tooltip, button, customElement } = action;
								const { title, placement = 'top' } = (tooltip || {});
								const { text, ...buttonProps } = (button || {});

								return (
									<Col key={idx}>
										{
											!button && customElement ?
												<div>
													{ customElement }
												</div>
											:
												<DesktopTooltip title={title} placement={placement}>
													{text && text.length ? (
														<Button {...buttonProps}>{text}</Button>
													) : (
														<Button {...buttonProps} />
													)}
												</DesktopTooltip>
										}
									</Col>
								);
							})}
						</Row>
					) : null}
				</Col>
			</Row>
		</div>
	);
};

export { View };
