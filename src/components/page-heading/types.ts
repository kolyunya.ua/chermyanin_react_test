import React from 'react';
import { ButtonHTMLType, ButtonShape, ButtonType } from 'antd/lib/button/button';
import { TooltipProps } from 'antd/lib/tooltip';

/**
 * IOwnProps
 * */
export interface IPageHeadingAction {
	tooltip?: TooltipProps;
	button: {
		type: ButtonType;
		htmlType: ButtonHTMLType;
		icon: string;
		text?: string;
		loading?: boolean,
		shape?: ButtonShape;
		className?: string;
		onClick(): void;
	} | null,
	customElement?: React.ReactNode;
}
export interface IOwnProps {
	title: React.ReactNode;
	actions?: IPageHeadingAction[];
}



/**
 * TConnectProps
 * */
export interface IMapStateToProps {}
export interface IMapDispatchToProps {}
export type TConnectProps = IMapStateToProps & IMapDispatchToProps;



/**
 * TComponentProps
 * */
export type TComponentProps = IOwnProps & TConnectProps;
