import React from 'react';
import { SpinProps } from 'antd/lib/spin';

/**
 * Common
 * */
export type TPreloaderTypes = 'spin' | 'linear';



/**
 * IOwnProps
 * */
export interface IOwnProps extends SpinProps {
	children?: React.ReactNode;
	type?: TPreloaderTypes;
	hideChildren?: boolean;
	centered?: boolean;
}



/**
 * TConnectProps
 * */
export interface IMapStateToProps {}
export interface IMapDispatchToProps {}
export type TConnectProps = IMapStateToProps & IMapDispatchToProps;



/**
 * TComponentProps
 * */
export type TComponentProps = IOwnProps & TConnectProps;
