import React from 'react';
import classnames from 'classnames';
import { Spin } from 'antd';
import { Box, LinearProgress } from '../../components';
import { makeClasses } from 'utils';
import { TComponentProps } from './types';

const View: React.FC<TComponentProps> = (props) => {
	const { children, type, hideChildren = false, centered = false, className = '', ...spinProps } = props;
	const isLinear = type === 'linear';
	const classes = makeClasses('preloader', [
		centered ? 'centered' : '',
		isLinear ? 'linear' : '',
	]);
	const rootClasses = classnames(classes, className);

	return (
		<div className={rootClasses}>
			<Spin
				{...spinProps}
				indicator={isLinear ? <Box><LinearProgress /></Box> : undefined}
			>
				{hideChildren ? null : children}
			</Spin>
		</div>
	);
};

export { View };
