import React from 'react';
import { Layout } from 'antd';
import { APP_NAME } from '../../../constants/env';
import { TComponentProps } from './types';

const View: React.FC<TComponentProps> = () => {
	return (
		<Layout.Footer className={'app-footer'}>
			{new Date().getFullYear()} &copy; {APP_NAME}
		</Layout.Footer>
	);
};

export { View };
