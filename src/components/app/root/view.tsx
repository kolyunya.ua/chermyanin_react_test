/*eslint-disable react-hooks/exhaustive-deps*/
import React from 'react';
import Helmet from 'react-helmet';
import { ConfigProvider as AntdConfigProvider } from 'antd';
import { APP_NAME } from 'constants/env';
import { RoutesSwitch } from 'routes/routes-switch';
import { makeClasses } from 'utils/common';
import { Progressbar } from 'components';
import { TComponentProps } from './types';
import backDrop from 'assets/images/backdrop.jpg';
import 'assets/scss/index.scss';
import 'assets/scss/common.scss';

const rootStyles = { backgroundImage: `url(${backDrop})` };

const View: React.FC<TComponentProps> = (props) => {
	const { device } = props;
	const classes = makeClasses('app-root', [ device.isMobile ? 'mobile' : 'desktop' ]);

	return (
		<AntdConfigProvider>
			<div className={classes} style={rootStyles}>
				<Progressbar />
				<Helmet defaultTitle={`${APP_NAME}`} titleTemplate={`%s | ${APP_NAME}`}/>
				<RoutesSwitch />
			</div>
		</AntdConfigProvider>
	);
};

export { View };
