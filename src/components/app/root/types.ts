import { IAction } from '../../../store/types';
import { IDevice } from '../../../store/app/types';
import { IStore as IProfile } from '../../../store/profile/types';

/**
 * IOwnProps
 * */
export interface IOwnProps {}



/**
 * TConnectProps
 * */
export interface IMapStateToProps {
	device: IDevice;
	profile: IProfile;
}
export interface IMapDispatchToProps {}
export type TConnectProps = IMapStateToProps & IMapDispatchToProps;



/**
 * TComponentProps
 * */
export type TComponentProps = IOwnProps & TConnectProps;
