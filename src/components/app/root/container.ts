import { compose } from 'redux';
import { connect } from 'react-redux';


import { IStoreState } from 'store/types';
import { getDevice } from 'store/app/selectors';
import { getProfile } from 'store/profile/selectors';
// import { fetchCommonInfoAll } from 'store/common-info/actions';
import { IMapStateToProps, IMapDispatchToProps } from './types';
import { View } from './view';

const mapStateToProps = (state: IStoreState): IMapStateToProps => {
	return {
		device: getDevice(state),
		profile: getProfile(state),
	};
};

const mapDispatchToProps: IMapDispatchToProps = { };

const Root = compose(
	connect(mapStateToProps, mapDispatchToProps)
)(View);

export { Root };
