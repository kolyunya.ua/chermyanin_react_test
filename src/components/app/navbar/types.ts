export interface INavLinks {
    key: string,
    name: string
} 

/**
 * IOwnProps
 * */
export interface IOwnProps {
    isAuthenticated: boolean;
}



/**
 * TConnectProps
 * */
export interface IMapStateToProps {}
export interface IMapDispatchToProps {

}

export type TConnectProps = IMapStateToProps & IMapDispatchToProps;



/**
 * TComponentProps
 * */
export type TComponentProps = IOwnProps & TConnectProps;
