import { compose } from 'redux';
import { connect } from 'react-redux';
import { View } from './view';
import { IStoreState } from 'store/types';
import { IMapStateToProps } from './types';

const mapStateToProps = (state: IStoreState): IMapStateToProps => {
	return {};
};

const mapDispatchToProps = null;

const NavBar = compose(
	connect(mapStateToProps, mapDispatchToProps),
)(View);

export { NavBar };
