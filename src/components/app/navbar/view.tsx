import './style.scss';
import React, { useMemo } from 'react';
import { NavLink } from 'react-router-dom';
// import { routes } from 'routes';
import { INavLinks, TComponentProps } from './types';

const renderMenuLinks = (links: INavLinks[], isAuthenticated: boolean = false) => {
	return links.map((route) => {
		if (!route) return route;

		if (!isAuthenticated) {
			return (
				<li key={route.key} className={'navbar__item'}>{route.name}</li>
			);
		} else {
			const { name, key } = route;
			const text = name || 'No name';
			return (
				<li key={route.key} className={'navbar__item'}>
					<NavLink to={key} className={'navbar__link'}>{text}</NavLink>
				</li>
			)
		}
	}).filter(Boolean);
};

const View: React.FC<TComponentProps> = (props) => {
	const navbarRoutes = useMemo(() => [
		{ name: 'Home', key: 'home' },
		{ name: 'Contacts', key: 'contacts' }
	], []);
	const { isAuthenticated } = props;
	return (
		<div className={'navbar'}>
			<ul className={'navbar__list'}>
				{renderMenuLinks(navbarRoutes, isAuthenticated)}
			</ul>
		</div>
	);
};

export { View };
