import './style.scss';
import React from 'react';
import { Button, Col, Row } from 'antd';
import { LoginOutlined } from '@ant-design/icons';
import { Logo } from '../logo';
import { NavBar } from '../navbar';
import { HeaderDropdown } from './components/header-dropdown';
import { TComponentProps } from './types';
import { FormLogin } from 'components';

const View: React.FC<TComponentProps> = (props) => {
	const { isAuthenticated } = props;
	return (
		<div className={'header'}>
			<Row gutter={36} align={'middle'}>
				<Col>
					<Logo />
				</Col>
				<Col className={'_flex-grow'}>
					<Row gutter={16} align={'middle'}>
						<Col className={'_flex-grow'}>
							<NavBar isAuthenticated={isAuthenticated} />
						</Col>
						{!isAuthenticated &&
							<Col>
								<FormLogin>
									<Button
										type={'link'}
										htmlType={'button'}
										icon={<LoginOutlined />}
									>
										Sign In
									</Button>
								</FormLogin>
							</Col>
						}
						{isAuthenticated &&
							<Col>
								<HeaderDropdown />
							</Col>
						}
					</Row>
				</Col>
			</Row>
		</div>
	);
};

export { View };
