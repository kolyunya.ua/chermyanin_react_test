import React from 'react';
import { Menu, Avatar, Row, Col } from 'antd';
import { routes } from 'routes/routes-tree';
import { redirect } from 'utils/redirect';
import { DownOutlined, LogoutOutlined, UserOutlined } from '@ant-design/icons'
import { TComponentProps } from './types';

const View: React.FC<TComponentProps> = (props) => {
	const { profile, clearSeed } = props;
	const { full_name, picture = {} } = profile;
	const { thumbnail } = picture;

	const menu = [
		<Menu.Item key={routes['profile'].link()}><UserOutlined />{routes['profile'].name}</Menu.Item>,
		<Menu.Divider key={'divider'} className={'header-dropdown__logout-divider'} />,
		<Menu.Item key={routes['logout'].link()}><LogoutOutlined />{routes['logout'].name}</Menu.Item>,
	];

	return (
		<div
			id={'header-dropdown'}
			className={'header-dropdown'}
		>
			<div className="header-dropdown__list">
				<Menu
					mode={'horizontal'}
					onClick={(link: any) => /^logout$/i.test(link.key) ? clearSeed() : redirect(link.key)}
					getPopupContainer={() => document.querySelector('#header-dropdown')! as HTMLElement}
				>
					<Menu.SubMenu
						title={
							<Row className={'_flex-nowrap'} gutter={12} align={'middle'}>
									<Col>
										<div className={'header-dropdown__title'}>
											<span>{'Hello!'} {full_name}&nbsp;<DownOutlined /></span>
										</div>
									</Col>
								<Col>
									<Avatar
										className={'header-dropdown__avatar'}
										src={thumbnail || undefined}
										size={46}
										icon={<UserOutlined />}
									/>
								</Col>
							</Row>
						}
					>
						{menu.filter(Boolean)}
					</Menu.SubMenu>
				</Menu>
			</div>
		</div>
	);
};

export { View };
