import { IAction } from 'store/types';
import { IDevice } from 'store/app/types';
import { IStore as IProfile } from 'store/profile/types';

/**
 * IOwnProps
 * */
export interface IOwnProps {}



/**
 * TConnectProps
 * */
export interface IMapStateToProps {
	profile: any;
	device: IDevice;
}
export interface IMapDispatchToProps {
	clearSeed(): IAction;
}
export type TConnectProps = IMapStateToProps & IMapDispatchToProps;



/**
 * TComponentProps
 * */
export type TComponentProps = IOwnProps & TConnectProps;
