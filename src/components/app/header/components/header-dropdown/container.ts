import { compose } from 'redux';
import { connect } from 'react-redux';
import { IStoreState } from 'store/types';
import { clearSeed } from 'store/auth/actions';
import { getDevice } from 'store/app/selectors';
import { getProfile } from 'store/profile/selectors';
import { IMapStateToProps, IMapDispatchToProps } from './types';
import { View } from './view';

const mapStateToProps = (state: IStoreState): IMapStateToProps => {
	return {
		profile: getProfile(state),
		device: getDevice(state),
	};
};

const mapDispatchToProps: IMapDispatchToProps = { clearSeed };

const HeaderDropdown = compose(
	connect(mapStateToProps, mapDispatchToProps)
)(View);

export { HeaderDropdown };
