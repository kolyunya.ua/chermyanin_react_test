import { compose } from 'redux';
import { connect } from 'react-redux';
import { IStoreState } from 'store/types';
import { IMapStateToProps, IMapDispatchToProps } from './types';
import { View } from './view';
import { getAuthIsAuthenticated } from 'store/auth/selectors';

const mapStateToProps = (state: IStoreState): IMapStateToProps => {
	return {
		isAuthenticated: getAuthIsAuthenticated(state)
	};
};

const mapDispatchToProps: IMapDispatchToProps = {};

const Header = compose(
	connect(mapStateToProps, mapDispatchToProps)
)(View);

export { Header };
