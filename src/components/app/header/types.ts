/**
 * IOwnProps
 * */
export interface IOwnProps {}



/**
 * TConnectProps
 * */
export interface IMapStateToProps {
    isAuthenticated: boolean;
}
export interface IMapDispatchToProps {}
export type TConnectProps = IMapStateToProps & IMapDispatchToProps;



/**
 * TComponentProps
 * */
export type TComponentProps = IOwnProps & TConnectProps;
