import { IAction } from 'store/types';
import {IContactsCollectionFilters} from 'store/contacts/types';

export interface IOwnProps {}

export interface IMapStateToProps {
	filters: IContactsCollectionFilters
}

export interface IMapDispatchToProps {
	fetchContactsCollection(payload: any): IAction<any>;
}

export type TComponentProps = IOwnProps & IMapDispatchToProps & IMapStateToProps;
