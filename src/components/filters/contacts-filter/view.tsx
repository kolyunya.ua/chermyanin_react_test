import React from 'react'
import { Col, Row, Button, Form, Input } from 'antd';
import { Storage } from 'services'
import debounce from "lodash.debounce";
import {IStoreState} from 'store/types';
import { getContactsFilters } from 'store/contacts/selectors';
import {connect} from 'react-redux';
import {compose} from 'redux';
import {IMapDispatchToProps, TComponentProps, IMapStateToProps} from './types';
import {CloseOutlined} from '@ant-design/icons';
import {Box} from 'components';
import { initialState } from 'store/contacts/reducer';
import { fetchContactsCollection } from 'store/contacts/actions';

const View: React.FC<TComponentProps> = (props) => {
	const {fetchContactsCollection, filters } = props;
	const [form] = Form.useForm();
	const seed = Storage.get('seed')

	const onChange = debounce((allValues) => {
		const {name, ...rest} = allValues;
		fetchContactsCollection( { params: {
			...filters,
			...rest,
			page: 1,
			results: 100,
			name: name,
			seed: seed
		} });
	}, 300);

	const onClear = () => {
		form.resetFields();
		fetchContactsCollection({params: initialState.collection.filters});
	};

	return (
		<Box className={'_mb-sm'}>
			<Form form={form} className="form form--filter" onValuesChange={onChange}>
				<Row className={'_sm-flex-nowrap _justify-around _sm-justify-between _s-sm'} align={'middle'}>
					<Col className={'_flex-grow'}>
						<Row className={'_s-sm'}>
							<Col span={24} md={8}>
								<Form.Item name="name">
									<Input placeholder={'Name, contact or address'} size="large" allowClear={true}/>
								</Form.Item>
							</Col>
						</Row>
					</Col>
					<Col className={'_flex-noshrink'}>
						<Button
							danger
							type={'link'}
							icon={<CloseOutlined/>}
							onClick={onClear}
						>
							{'Clear'}
						</Button>
					</Col>
				</Row>
			</Form>
		</Box>
	)
};

const mapStateToProps = (state: IStoreState): IMapStateToProps => {
	return {
		filters: getContactsFilters(state)
	}
};

const mapDispatchToProps: IMapDispatchToProps = {
	fetchContactsCollection
};

const ContactsFilter = compose<React.FC>(
	connect(mapStateToProps, mapDispatchToProps)
)(View);

export { ContactsFilter };
