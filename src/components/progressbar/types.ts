/**
 * IOwnProps
 * */
export interface IOwnProps {}



/**
 * TConnectProps
 * */
export interface IMapStateToProps {
	hasAnyPreloader: boolean;
}
export interface IMapDispatchToProps {}
export type TConnectProps = IMapStateToProps & IMapDispatchToProps;



/**
 * TComponentProps
 * */
export type TComponentProps = IOwnProps & TConnectProps;
