import { compose } from 'redux';
import { connect } from 'react-redux';
import { IStoreState } from 'store/types';
import { hasAnyPreloader } from 'store/preloader/selectors';
import { IMapStateToProps, IMapDispatchToProps } from './types';
import { View } from './view';

const mapStateToProps = (state: IStoreState): IMapStateToProps => {
	return {
		hasAnyPreloader: hasAnyPreloader(state),
	};
};

const mapDispatchToProps: IMapDispatchToProps = {};

const Progressbar = compose(
	connect(mapStateToProps, mapDispatchToProps)
)(View);

export { Progressbar };
