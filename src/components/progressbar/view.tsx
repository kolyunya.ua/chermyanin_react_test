import React from 'react';
import { LinearProgress } from '../../components';
import { TComponentProps } from './types';

const View: React.FC<TComponentProps> = ({ hasAnyPreloader }) => {
	const classes = ['app-progressbar', hasAnyPreloader ? 'is-active' : ''];

	return (
		<div id={'app-progressbar'} className={classes.join(' ')} >
			<LinearProgress />
		</div>
	);
};

export { View };
