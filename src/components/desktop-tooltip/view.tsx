import React, { Fragment } from 'react';
import { Tooltip } from 'antd';
import { TComponentProps } from './types';

const View: React.FC<TComponentProps> = (props) => {
	const { device, children, ...tooltipProps } = props;

	return device.isDesktop
		? <Tooltip {...tooltipProps} arrowPointAtCenter={true}>{children}</Tooltip>
		: <Fragment>{children}</Fragment>;
};

export { View };
