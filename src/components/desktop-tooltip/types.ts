import { TooltipProps } from 'antd/lib/tooltip';
import { IDevice } from 'store/app/types';

/**
 * IOwnProps
 * */
export interface IOwnProps {}



/**
 * TConnectProps
 * */
export interface IMapStateToProps {
	device: IDevice;
}
export interface IMapDispatchToProps {}
export type TConnectProps = IMapStateToProps & IMapDispatchToProps;



/**
 * TComponentProps
 * */
export type TComponentProps = TooltipProps & IOwnProps & TConnectProps;
