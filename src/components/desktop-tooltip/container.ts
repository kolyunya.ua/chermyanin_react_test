import { compose } from 'redux';
import { connect } from 'react-redux';
import { IStoreState } from 'store/types';
import { getDevice } from 'store/app/selectors';
import { IMapStateToProps, IMapDispatchToProps } from './types';
import { View } from './view';

const mapStateToProps = (state: IStoreState): IMapStateToProps => {
	return {
		device: getDevice(state),
	};
};

const mapDispatchToProps: IMapDispatchToProps = {};

const DesktopTooltip = compose(
	connect(mapStateToProps, mapDispatchToProps)
)(View);

export { DesktopTooltip };
