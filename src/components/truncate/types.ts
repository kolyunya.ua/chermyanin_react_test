import React from 'react';

/**
 * Common
 * */
export type TCopyTypes = 'email' | 'fax' | 'address' | 'text';
export interface IEllipsis {
	rows?: number;
	expandable?: boolean;
	onExpand?: () => void;
}



/**
 * IOwnProps
 * */
export interface IOwnProps {
	key?: string;
	type: TCopyTypes;
	copy?: string;
	ellipsis?: IEllipsis;
	className?: string;
	children: React.ReactNode;
}



/**
 * TConnectProps
 * */
export interface IMapStateToProps {}
export interface IMapDispatchToProps {}
export type TConnectProps = IMapStateToProps & IMapDispatchToProps;



/**
 * TComponentProps
 * */
export type TComponentProps = IOwnProps & TConnectProps;
