import React from 'react';

export interface IPhone {
	number: string;
	extension?: string;
}



/**
 * IOwnProps
 * */
export interface IOwnProps {
	key?: string;
	phone: IPhone;
	copy?: string;
	className?: string;
	defaultValue?: React.ReactNode;
}



/**
 * TConnectProps
 * */
export interface IMapStateToProps {}
export interface IMapDispatchToProps {}
export type TConnectProps = IMapStateToProps & IMapDispatchToProps;



/**
 * TComponentProps
 * */
export type TComponentProps = IOwnProps & TConnectProps;
