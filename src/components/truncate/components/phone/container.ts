import { compose } from 'redux';
import { connect } from 'react-redux';
import { IStoreState } from 'store/types';
import { IMapStateToProps, IMapDispatchToProps } from './types';
import { View } from './view';

const mapStateToProps = (state: IStoreState): IMapStateToProps => {
	return {};
};

const mapDispatchToProps: IMapDispatchToProps = {};

const Phone = compose(
	connect(mapStateToProps, mapDispatchToProps)
)(View);

export { Phone };
