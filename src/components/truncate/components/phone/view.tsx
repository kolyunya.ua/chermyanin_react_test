import React, { Fragment } from 'react';
import classnames from 'classnames';
import { Typography, Row, Col } from 'antd';
import { LEAVE_NUMBERS_ONLY } from 'constants/regexp';
import { makeClasses } from 'utils';
import { TComponentProps } from './types';

const View: React.FC<TComponentProps> = (props) => {
	const { key, phone, className, copy, defaultValue = null } = props;
	const classes = makeClasses('truncate', ['phone']);
	const rootClasses = classnames(classes, className);
	const preparedPhone = (phone.number || '').replace(LEAVE_NUMBERS_ONLY, '');
	const phoneExtension = phone.extension;

	return preparedPhone ? (
		<div key={key} className={rootClasses}>
			<Row className={'_flex-nowrap'} gutter={8}>
				<Col className={'truncate__icon-col'}>
					<Typography.Text className={'truncate__icon'} copyable={{ text: copy || phone.number }} />
				</Col>
				<Col className={'truncate__content-col'}>
					<Typography.Paragraph className={'truncate__content'} ellipsis={true}>
						<a href={`tel:+${preparedPhone}`}>{phone.number}</a>
						{phoneExtension ? <span className={'_ml-xs'}>({phoneExtension})</span> : null}
					</Typography.Paragraph>
				</Col>
			</Row>
		</div>
	) : <Fragment>{defaultValue}</Fragment>;
};

export { View };
