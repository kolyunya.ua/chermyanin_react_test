import React from 'react';
import classnames from 'classnames';
import { Typography, Row, Col } from 'antd';
import { makeClasses } from 'utils';
import { Phone } from './components/phone';
import { IEllipsis, TComponentProps } from './types';

const ellipsisDefaults: IEllipsis = {
	rows: 3,
	expandable: false,
	onExpand: () => {},
};

const View: React.FC<TComponentProps> & { Phone: typeof Phone } = (props) => {
	const { key, type, copy, children, className } = props;
	const { ellipsis = ellipsisDefaults } = props;
	const classes = makeClasses('truncate', [type]);
	const rootClasses = classnames(classes, className);

	return children ? (
		<div key={key} className={rootClasses}>
			<Row className={'_flex-nowrap'} gutter={8}>
				{!!copy && (
					<Col className={'truncate__icon-col'}>
						<Typography.Text className={'truncate__icon'} copyable={{ text: copy }} />
					</Col>
				)}
				<Col className={'truncate__content-col'}>
					{type === 'email' && (
						<Typography.Paragraph className={'truncate__content'} ellipsis={true}>
							<a href={`mailto:${copy}`}>{children}</a>
						</Typography.Paragraph>
					)}
					{type === 'fax' && (
						<Typography.Paragraph className={'truncate__content'} ellipsis={true}>
							<span>{children}</span>
						</Typography.Paragraph>
					)}
					{type === 'address' && (
						<Typography.Paragraph className={'truncate__content'} ellipsis={ellipsis}>
							<span>{children}</span>
						</Typography.Paragraph>
					)}
					{type === 'text' && (
						<Typography.Paragraph className={'truncate__content'} ellipsis={ellipsis}>
							<span>{children}</span>
						</Typography.Paragraph>
					)}
				</Col>
			</Row>
		</div>
	) : null;
};

View.Phone = Phone;

export { View };
