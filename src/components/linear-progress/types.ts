/**
 * IOwnProps
 * */
export interface IOwnProps {
	direction?: 'forward' | 'backward';
}



/**
 * TConnectProps
 * */
export interface IMapStateToProps {}
export interface IMapDispatchToProps {}
export type TConnectProps = IMapStateToProps & IMapDispatchToProps;



/**
 * TComponentProps
 * */
export type TComponentProps = IOwnProps & TConnectProps;
