import React from 'react';
import { TComponentProps } from './types';

const View: React.FC<TComponentProps> = ({ direction = 'forward' }) => {
	const classes = [
		'linear-progress',
		direction === 'forward'
			? 'linear-progress--forward'
			: 'linear-progress--backward',
	];

	return (
		<div className={classes.join(' ')} role={'progressbar'}>
			<div className={'linear-progress__bar linear-progress__bar-first'} />
			<div className={'linear-progress__bar linear-progress__bar-second'} />
		</div>
	);
};

export { View };
