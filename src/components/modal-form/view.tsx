/*eslint-disable react-hooks/exhaustive-deps*/
import React, { useEffect, useState } from 'react';
import { Form, Modal} from 'antd';
import debounce from 'lodash.debounce';

export const ModalForm = (props: any) => {
	const {
		button,
		content,
		pending,
		handler,
		titleConfirm,
		isConfirm,
		initialValues,
		okText,
		placement,
		validate,
		cancelText,
		customButtons,
		...rest
	} = props;
	const [form] = Form.useForm();
	const [visible, setVisible] = useState(false);

	useEffect(() => {
		return () => {
			form.resetFields();
		}
	}, [initialValues]);

	const onCancel = () => {
		form.resetFields();
		setVisible(false);
	};

	const onValuesChange = () => {
		const values = form.getFieldsValue();
		debounce(validate(values, form), 300);
	};

	const onSubmit = async () => {
		const values = form.getFieldsValue();
		if (!!validate) {
			const isErrors = await validate(values, form, true);
			if (!isErrors) {
				handler(values);
				form.resetFields();
				onCancel();
			}
		} else {
			handler(values);
			form.resetFields();
			onCancel();
		}
	};

	return (
		<>
			<span onClick={() => setVisible(true)}>
				{button}
			</span>
			<Modal
				centered={true}
				width={520}
				visible={visible}
				closable={false}
				{...rest}
			>
				<Form
					layout="vertical"
					className="form"
					form={form}
					onFinish={onSubmit}
					autoComplete="off"
					onValuesChange={!!validate ? onValuesChange : undefined}
				>
					{content(form, onCancel)}
				</Form>
			</Modal>
		</>
	);
};
