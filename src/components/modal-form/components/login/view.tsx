import React from 'react';
import { Button, Col, Form, Input, Row } from 'antd';
import { ModalForm } from 'components';
import { TComponentProps } from './types';
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { ILoginRequestPayload } from 'store/auth/types';
import { makeClasses } from 'utils';

const View: React.FC<TComponentProps> = (props) => {
	const {
		isLoggingIn,
		children,
		loginRequest
	} = props;

	const formClassNames = makeClasses('form', ['vertical', 'login']);
	const [form] = Form.useForm();

	const onSubmit = async (values: any) => {
		loginRequest(values as ILoginRequestPayload);
	};

	return (
		<ModalForm
			title={'Sign in'}
			handler={onSubmit}
			pending={isLoggingIn}
			button={children}
			form={form}
			className={formClassNames}
			content={(from: any, onCancel: Function) => {
				return (
					<React.Fragment>
						<div className={'form__body'}>
							<Row className="_s-sm">
								<Col span={24}>
									<Form.Item name="email">
										<Input type="email" size="large" allowClear={true} placeholder={'Email'} prefix={<UserOutlined />} />
									</Form.Item>
								</Col>
								<Col span={24}>
									<Form.Item name="password">
										<Input type="password" size="large" allowClear={true} placeholder={'Password'} prefix={<LockOutlined />} />
									</Form.Item>
								</Col>
							</Row>
						</div>
						<div className={'form__footer'}>
							<Row gutter={[20, 0]}>
								<Col span={12}>
									<Button
										type={'primary'}
										htmlType={'submit'}
										className={'control control--submit _flex-grow'}
										loading={isLoggingIn}
										size={'large'}
										block={true}
									>
										{'Sign In'}
									</Button>
								</Col>
								<Col span={12}>
									<Button
										type={'default'}
										htmlType={'submit'}
										className={'control control--submit'}
										loading={isLoggingIn}
										size={'large'}
										onClick={() => onCancel()}
										block={true}
									>
										{'Cancel'}
									</Button>
								</Col>
							</Row>
						</div>
					</React.Fragment>
				);
			}}
		/>
	);
};

export { View };
