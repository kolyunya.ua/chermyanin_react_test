import { IMapStateToProps, IMapDispatchToProps } from './types';
import { IStoreState } from 'store/types';
import { getAuthIsLoggingIn } from 'store/auth/selectors';
import { loginRequest } from 'store/auth/actions';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { View } from './view';


const mapStateToProps = (state: IStoreState): IMapStateToProps => {
	return {
		isLoggingIn: getAuthIsLoggingIn(state),
	};
};

const mapDispatchToProps: IMapDispatchToProps = { loginRequest };

const FormLogin = compose<any>(
	connect(mapStateToProps, mapDispatchToProps),
)(View);

export { FormLogin };
