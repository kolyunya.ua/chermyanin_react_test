import { IAction } from 'store/types';
import { ILoginRequestPayload } from 'store/auth/types';

/**
 * IOwnProps
 * */
export interface IOwnProps {
	children: any
}



/**
 * TConnectProps
 * */
export interface IMapStateToProps {
	isLoggingIn: boolean;
}
export interface IMapDispatchToProps {
	loginRequest(payload?: any): IAction<any>;
}
export type TConnectProps = IMapStateToProps & IMapDispatchToProps;



/**
 * TComponentProps
 * */
export type TComponentProps = IOwnProps & TConnectProps;



/**
 * TFormProps
 * */
export enum EFormFieldNames {
	email = 'email',
	password = 'password',
}
export type TFormFields = {
	[fieldName in EFormFieldNames]: string;
}
export type TFormProps = TComponentProps;
