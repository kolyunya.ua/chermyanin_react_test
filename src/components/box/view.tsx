import React from 'react';
import { TComponentProps } from './types';

const View: React.FC<TComponentProps> = (props) => {
	const { children } = props;

	return (
		<div className={'box'}>
			<div className={'box__content'}>{children}</div>
		</div>
	);
};

export { View };
