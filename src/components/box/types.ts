import React from 'react';

/**
 * IOwnProps
 * */
export interface IOwnProps {
	modifiers?: string | string[];
	className?: string;
	children: React.ReactNode;
}



/**
 * TConnectProps
 * */
export interface IMapStateToProps {}
export interface IMapDispatchToProps {}
export type TConnectProps = IMapStateToProps & IMapDispatchToProps;



/**
 * TComponentProps
 * */
export type TComponentProps = IOwnProps & TConnectProps;
