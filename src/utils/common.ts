
import { PlainObject } from 'definitions/common-types';
import { isValidReactElement } from './react-checkers';


export const makeClasses = (rootClass: string, modifiers: string | string[] = []): string => {
	modifiers = Array.isArray(modifiers) ? modifiers : [modifiers];

	const modifierClasses: string[] = modifiers.map((modifier: string) => modifier ? `${rootClass}--${modifier}` : '');

	return [
		rootClass,
		...modifierClasses
	].join(' ');
};

type TWithDefaultReturnedFunction = (content: React.ReactNode) => React.ReactNode;
export const withDefault = (value: string = 'N/a'): TWithDefaultReturnedFunction => {
	return (content) => {
		if (content == null) return value;
		const _isValidReactElement = (Array.isArray(content) ? content : [content]).every((element) => isValidReactElement(element));
		return _isValidReactElement ? content : String(content);
	};
};

export const removeProps = <S extends {}>(source: PlainObject, props: string | string[]) => {
	props = Array.isArray(props) ? props : [props];
	const rest = props.reduce<PlainObject>((prevSource, nextProp) => {
		const { [nextProp]: _, ...nextSource } = prevSource;
		return nextSource;
	}, source);
	return rest as S;
};

export const makeFullName = (title: string, firstName: string, lastName: string) => {
	return `${title}. ${firstName} ${lastName}`
}

export const makeAddress = (streetNumber: number, streetName: string, city: string, state: string, postcode: number) => {
	return `${streetNumber || ''} ${streetName || ''}, ${city || ''}, ${postcode || ''} ${state || ''}`
}

