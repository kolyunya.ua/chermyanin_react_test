import {
	ComponentClass,
	FunctionComponent,
	ComponentType,
	ReactNode,
	ReactElement,
	isValidElement,
} from 'react';


export const isReactClassComponent = (component: ComponentClass) => {
	return typeof component === 'function' && !!component.prototype.isReactComponent;
};

export const isReactFunctionComponent = (component: FunctionComponent) => {
	return typeof component === 'function' && String(component).includes('return createElement');
};

export const isReactComponent = (component: ComponentType) => {
	return isReactClassComponent(component as ComponentClass) || isReactFunctionComponent(component as FunctionComponent);
};

export const isValidReactElement = (element: ReactNode) => {
	return isValidElement(element);
};

export const isValidReactDOMTypeElement = (element: ReactElement) => {
	return isValidReactElement(element) && typeof element.type === 'string';
};

export const isValidReactCompositeTypeElement = (element: ReactElement) => {
	return isValidReactElement(element) && typeof element.type === 'function';
};
