import { history } from '../routes/history';

// broksea/src/Utils/redirect/index.ts
export const redirect = (link: string, replace?: boolean): void => {
	if (replace) {
		history.replace(link);
	}
	history.push(link);
};
