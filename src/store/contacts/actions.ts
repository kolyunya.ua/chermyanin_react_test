import { createAction } from '../utils';
import { IQueryParams } from '../types';
import {
	IContactRecord,
	IContactsCollectionList,
	IContactsCollectionFilters,
	IFetchContactsRequestPayload,
} from './types';

export enum ETypes {
	FETCH_CONACTS_COLLECTION 		= '@contacts/FETCH_CONACTS_COLLECTION',
	SAVE_CONACTS_LIST 				= '@contacts/SAVE_CONACTS_LIST',
	SET_CONTACTS_FILTERS 			= '@contacts/SET_CONTACTS_FILTERS',

	FETCH_CONTACT 					= '@contacts/FETCH_CONTACT',
	SAVE_CONTACT 					= '@contacts/SAVE_CONTACT',

	SEARCH_CONTACT 					= '@contacts/SEARCH_CONTACT',
	SAVE_FOUNDED_CONTACTS 			= '@contacts/SAVE_FOUNDED_CONTACTS',

	CREATE_CONTACT 					= '@contacts/CREATE_CONTACT',
	UPDATE_CONTACT 					= '@contacts/UPDATE_CONTACT',
	DROP_CONTACT 					= '@contacts/DROP_CONTACT',

	IMPORT_CONTACTS 				= "@contacts/IMPORT_CONTACTS"
}

export const fetchContactsCollection        = createAction<IQueryParams>(ETypes.FETCH_CONACTS_COLLECTION);
export const saveContactsList  				= createAction<IContactsCollectionList>(ETypes.SAVE_CONACTS_LIST);
export const setContactsFilters 			= createAction<IContactsCollectionFilters>(ETypes.SET_CONTACTS_FILTERS);


export const fetchContact 			= createAction<IFetchContactsRequestPayload>(ETypes.FETCH_CONTACT);
export const saveContact			= createAction<IContactRecord>(ETypes.SAVE_CONTACT);

export const searchContact 			= createAction<IQueryParams>(ETypes.SEARCH_CONTACT);
export const saveFoundedContacts	= createAction<IContactsCollectionList>(ETypes.SAVE_FOUNDED_CONTACTS);
