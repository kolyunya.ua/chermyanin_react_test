import { createReducer } from 'store/utils';
import { IAction } from 'store/types';
import { ETypes as contacts } from './actions';
import { IStore, IContactsCollectionList, IContactsCollectionFilters, IContactRecord } from './types';

export const initialState: IStore = {
	collection: {
		list: {
			results: [],
			info: {}
		},
		filters: {
			page: 1,
			per_page: undefined,
			order_by: undefined,
			order_type: undefined,
			name: undefined,
			type_id: undefined,
			results: 100
		},
	},
	records: {
		list: {},
	},
	search: {
		list: {
			results: [],
		}
	}
};

export const reducer = createReducer<IStore>(initialState, {
	[contacts.SAVE_CONACTS_LIST](state: IStore, action: IAction<IContactsCollectionList>) {
		const { payload } = action;

		return {
			...state,
			collection: {
				...state.collection,
				list: payload.results,
			}
		};
	},

	[contacts.SET_CONTACTS_FILTERS](state: IStore, action: IAction<IContactsCollectionFilters>) {
		const { payload } = action;

		return {
			...state,
			collection: {
				...state.collection,
				filters: {
					...state.collection.filters,
					...payload,
				},
			}
		};
	},

	[contacts.SAVE_CONTACT](state: IStore, action: IAction<IContactRecord>) {
		const { payload } = action;
		const id: any = payload.login.uuid;

		return {
			...state,
			records: {
				...state.records,
				list: {
					...state.records.list,
					[id]: payload,
				}
			}
		};
	},

	[contacts.SAVE_FOUNDED_CONTACTS](state: IStore, action: IAction<IContactsCollectionList>) {
		const { payload } = action;

		return {
			...state,
			search: {
				...state.search,
				list: payload,
			}
		};
	}
});
