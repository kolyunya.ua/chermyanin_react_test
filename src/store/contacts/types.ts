import { ID, PlainObject } from "definitions/common-types";
import { IFilters, IIncomingCollection } from "store/types";

export interface IContactAge {
	age: number;
	date: string;
}

export interface IContactName {
	first: string;
	last: string;
	title: string;
}

export interface IContactPicture {
	large: string;
	medium: string;
	thumbnail: string;
}

export interface IContactLocation {
	city: string;
	country: string;
	postcode: number;
	state: string;
	street: {
		name: string;
		number: number;
	};
}

export interface IContactRecord {
	dob: IContactAge;
	email: string;
	gender: string;
	login: {
		uuid: string;
	};
	name: IContactName;
	nat: string;
	phone: string;
	picture: IContactPicture;
	location: IContactLocation;
}

export interface IContactsCollectionList
	extends IIncomingCollection<IContactRecord> {}
export interface IContactsCollectionFilters extends IFilters {
	name?: string;
	type_id?: string;
	results: number
}

export interface IStore {
	collection: {
		list: IContactsCollectionList;
		filters: IContactsCollectionFilters;
	};
	records: {
		list: PlainObject<IContactRecord>;
	};
	search: {
		list: IContactsCollectionList;
	};
}

export interface IFetchContactsRequestPayload {
	seed: string;
}
