import { call, put, takeEvery } from 'redux-saga/effects';
import { api } from 'services';
import { IAction } from '../types';
import { workerMiddleware } from '../worker-middleware';
import {
	ETypes as contacts,
	saveContactsList,
	setContactsFilters,
} from './actions';;

function* fetchContactsWorker (action: IAction<any>) {
	const { payload = {} } = action;
	yield put(setContactsFilters(payload.params));
	const response: any = yield call(api.contacts.fetchContacts, payload);
	yield put(saveContactsList(response));
}





export function* saga() {
	yield takeEvery(contacts.FETCH_CONACTS_COLLECTION, workerMiddleware, { worker: fetchContactsWorker });
}
