import { createSelector } from 'reselect';
import { ID } from 'definitions/common-types';
import { IStore, IContactsCollectionList, IContactsCollectionFilters } from './types';
import { IStoreState } from 'store/types';
const _getContacts = (state: IStoreState): IStore => state.contacts;
const _getContactId = (state: IStoreState, id: ID): string => String(id);

export const getContacts = createSelector(
	[_getContacts],
	(contacts): IStore => contacts,
);

export const getContactsList = createSelector(
	[getContacts],
	(contacts): IContactsCollectionList => contacts.collection.list,
);

export const getContactsFilters = createSelector(
	[getContacts],
	(contacts): IContactsCollectionFilters => contacts.collection.filters,
);

export const getContactRecords = createSelector(
	[getContacts],
	(contacts) => contacts.records,
);

export const getContactData = createSelector(
	[getContactRecords, _getContactId],
	(records, id) => records.list[id] || {},
);

export const getFoundedContactsList = createSelector(
	[getContacts],
	(contacts): IContactsCollectionList => contacts.search.list,
);
