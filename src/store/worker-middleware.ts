import { call, put } from 'redux-saga/effects';
// import { onCatchError } from '../utils';
import { IAction } from '../store/types';
import { addPreloader, removePreloader } from './preloader/actions';

interface IParams {
	worker(action: IAction): any;
}

export function* workerMiddleware (params: IParams, action: IAction) {
	yield put(addPreloader({ preloaderName: action.type }));
	try {
		yield call(params.worker, action);
	} catch (error) {
		console.log({ error });
	} finally {
		yield put(removePreloader({ preloaderName: action.type }));
	}
}
