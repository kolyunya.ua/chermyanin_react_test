import { createStore, applyMiddleware, Store } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';
import createSagaMiddleware from 'redux-saga';
import { ALLOW_WINDOW_EXTENDS, ALLOW_REDUX_DEVTOOLS_EXTENSION } from '../constants/env';
import { createRootReducer } from './reducers';
import { sagas } from './sagas';
import { history } from '../routes/history';
import { IStoreState } from './types';
import { restoreAuth } from './restore-auth'

const INITIAL_STATE = window.__INITIAL_STATE__ || {};
const sagaMiddleware = createSagaMiddleware();
const reduxMiddleware = applyMiddleware(
	sagaMiddleware,
);
const enhancers = ALLOW_REDUX_DEVTOOLS_EXTENSION ? composeWithDevTools(reduxMiddleware) : reduxMiddleware;

const configureStore = (preloadedState = INITIAL_STATE): Store<IStoreState> => {
	return createStore(
		createRootReducer(history),
		preloadedState,
		enhancers,
	);
};

const store = configureStore();

sagaMiddleware.run(sagas);

export { store, restoreAuth };

if (ALLOW_WINDOW_EXTENDS) {
	window.$store = store;
}
