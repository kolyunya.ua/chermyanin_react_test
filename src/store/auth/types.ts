export interface IStore {
	isAuthenticated: boolean;
	isLoggingIn: boolean;
	seed: null | string;
}

export interface ILoginRequestPayload {
	email: string;
	password: string;
}

export interface ILoginSuccessPayload {
	seed: string;
}
