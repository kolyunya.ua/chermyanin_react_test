import { createSelector } from 'reselect';
import { IStoreState } from '../types';
import { IStore } from './types';

/*
|--------------------------------------------------------------------------
| State piece getters
|--------------------------------------------------------------------------
*/
const _getAuth = (state: IStoreState): IStore => state.auth;

/*
|--------------------------------------------------------------------------
| Selectors
|--------------------------------------------------------------------------
*/
export const getAuth = createSelector(
	[_getAuth],
	(auth: IStore): IStore => auth
);

export const getAuthIsAuthenticated = createSelector(
	[getAuth],
	(auth: IStore): boolean => auth.isAuthenticated,
);

export const getAuthIsLoggingIn = createSelector(
	[getAuth],
	(auth: IStore): boolean => auth.isLoggingIn,
);

export const getAuthSeed= createSelector(
	[getAuth],
	(auth: IStore): string | null => auth.seed,
);
