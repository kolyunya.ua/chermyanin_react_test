import { createReducer } from '../utils';
import { IAction } from '../types';
import { IStore, ILoginSuccessPayload } from './types';
import { ETypes as auth } from './actions';

export const initialState: IStore = {
	isAuthenticated: false,
	isLoggingIn: false,
	seed: null,
};

export const reducer = createReducer<IStore>(initialState, {
	[auth.LOGIN_REQUEST](state: IStore) {
		return {
			...state,
			isLoggingIn: true
		};
	},
	[auth.LOGIN_SUCCESS](state: IStore, action: IAction<ILoginSuccessPayload>) {
		const { payload } = action;
		console.log(payload)
		return {
			...state,
			seed: payload,
			isLoggingIn: false,
			isAuthenticated: true,
		};
	},
	[auth.LOGOUT_REQUEST](state: IStore) {
		return {
			...state,
			seed: '',
			isLoggingIn: false,
			isAuthenticated: false,
		};
	},
});
