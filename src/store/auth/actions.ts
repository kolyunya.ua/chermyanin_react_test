import { createAction } from '../utils';
import {
	ILoginSuccessPayload,
} from './types';

export enum ETypes {
	LOGIN_REQUEST 						= '@auth/LOGIN_REQUEST',
	LOGIN_SUCCESS 						= '@auth/LOGIN_SUCCESS',
	LOGOUT_SUCCESS 						= '@auth/LOGOUT_SUCCESS',
	LOGOUT_REQUEST						= '@auth/LOGOUT_REQUEST',
	CLEAR_SEED 							= '@auth/CLEAR_SEED'
}

export const loginRequest 				= createAction(ETypes.LOGIN_REQUEST);
export const loginSuccess 				= createAction<ILoginSuccessPayload>(ETypes.LOGIN_SUCCESS);
export const logoutRequest 				= createAction(ETypes.LOGOUT_REQUEST);
export const logoutSuccess 				= createAction(ETypes.LOGOUT_SUCCESS);
export const clearSeed 					= createAction(ETypes.LOGOUT_SUCCESS);
