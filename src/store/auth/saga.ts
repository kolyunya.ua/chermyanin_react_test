import { put, takeEvery } from "redux-saga/effects";
import {Storage } from "services";
import { workerMiddleware } from "../worker-middleware";
import { IAction } from "../types";
import { ETypes as auth, loginSuccess } from "./actions";
const removeSeedFromLocalStorage = () => Storage.remove("seed");

function* loginRequestWorker(action: IAction<any>) {
	const { payload } = action;
	try {
		Storage.set("seed", payload.email);
		yield put(loginSuccess(payload.email));
	} catch (error) {
		console.log("Error", error);
	}
}

function* logoutRequestWorker() {
	try {
		yield removeSeedFromLocalStorage()
	} catch (error) {
		console.log("Error", error);
	}
}

export function* saga() {
	yield takeEvery(auth.LOGIN_REQUEST, workerMiddleware, {
		worker: loginRequestWorker,
	});
	yield takeEvery(auth.LOGOUT_REQUEST, workerMiddleware, {
		worker: logoutRequestWorker,
	});
}
