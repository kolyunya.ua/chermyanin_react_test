import { createAction } from '../utils';
import { IModalOpenPayload } from './types';

export enum ETypes {
	OPEN 					= '@modal/OPEN',
	CLOSE 					= '@modal/CLOSE',
	DESTROY 				= '@modal/DESTROY',
}

export const openModal 		= createAction<IModalOpenPayload<any>>(ETypes.OPEN);
export const closeModal 	= createAction(ETypes.CLOSE);
export const destroyModal 	= createAction(ETypes.DESTROY);
