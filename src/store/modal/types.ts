import React from 'react';

export type TForwardProps<ForwardProps = {}> = ForwardProps & {
	title?: string;
};

export interface IPublicParams {
	className?: string;
	centered?: boolean;
	width?: string | number;
	style?: React.CSSProperties;
}

export interface IStore {
	name: null | string;
	forwardProps: TForwardProps;
	publicParams: IPublicParams;
	privateParams: {
		visible: boolean;
	}
}

export interface IModalOpenPayload<ForwardProps = {}> {
	name: string;
	publicParams?: IPublicParams;
	forwardProps?: TForwardProps<ForwardProps>;
}
