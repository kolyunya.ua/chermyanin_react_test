import { createSelector } from 'reselect';
import { IStoreState } from '../types';
import { IStore } from './types';

const _getModal = (state: IStoreState): IStore => state.modal;

export const getModal = createSelector(
	[_getModal],
	(modal: IStore): IStore => modal
);
