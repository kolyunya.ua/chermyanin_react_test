import { createReducer } from '../utils';
import { IAction } from '../types';
import { IStore, IModalOpenPayload } from './types';
import { ETypes as modal } from './actions';

export const initialState: IStore = {
	name: null,
	forwardProps: {},
	publicParams: {},
	privateParams: {
		visible: false,
	},
};

export const reducer = createReducer<IStore>(initialState, {
	[modal.OPEN](state: IStore, action: IAction<IModalOpenPayload>) {
		const { payload } = action;
		const { name, forwardProps = {}, publicParams = {} } = payload;
		return {
			...state,
			name,
			forwardProps,
			publicParams,
			privateParams: {
				...state.privateParams,
				visible: true,
			},
		};
	},
	[modal.CLOSE](state: IStore) {
		return {
			...state,
			privateParams: {
				...state.privateParams,
				visible: false,
			},
		};
	},
	[modal.DESTROY]() {
		return initialState;
	},
	'@@router/LOCATION_CHANGE'() {
		return initialState;
	},
});
