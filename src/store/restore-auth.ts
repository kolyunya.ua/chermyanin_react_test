import { Storage } from 'services';
import { store } from './index';
import { ILoginSuccessPayload } from './auth/types';
import { clearSeed, loginRequest } from './auth/actions';

export const restoreAuth = () => {
	const seed = Storage.get('seed') as ILoginSuccessPayload;
	if (seed) {
		store.dispatch(loginRequest(seed));
	} else {
		store.dispatch(clearSeed());
	}
};
