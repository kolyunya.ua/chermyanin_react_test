export interface IStore<T = string> {
	list: Set<T>;
}

export interface IAddPreloaderPayload {
	preloaderName: string;
}

export interface IRemovePreloaderPayload {
	preloaderName: string;
}
