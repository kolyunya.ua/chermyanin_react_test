import { createSelector } from 'reselect';
import { IStoreState } from '../types';
import { IStore } from './types';

const _getPreloaderState = (state: IStoreState): IStore => state.preloader;
const _preloaderNames = (state: IStoreState, preloaderNames: string | string[]): string[] => {
	if (!Array.isArray(preloaderNames)) {
		preloaderNames = [preloaderNames];
	}
	return preloaderNames;
};

export const getPreloaderState = createSelector(
	[_getPreloaderState],
	(preloader: IStore): IStore => preloader
);

export const getPreloadersList = createSelector(
	[getPreloaderState],
	(preloaderState: IStore): Set<string> => preloaderState.list
);

export const hasPreloader = createSelector(
	[getPreloadersList, _preloaderNames],
	(list, preloaderNames): boolean => preloaderNames.some((name) => list.has(name))
);

export const hasAnyPreloader = createSelector(
	[getPreloadersList],
	(list): boolean => list.size > 0
);
