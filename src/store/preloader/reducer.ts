import { createReducer } from '../utils';
import { IAction } from '../types';
import { IStore, IAddPreloaderPayload, IRemovePreloaderPayload } from './types';
import { ETypes as preloader } from './actions';

export const initialState: IStore = {
	list: new Set<string>(),
};

export const reducer = createReducer<IStore>(initialState, {
	[preloader.ADD](state: IStore, action: IAction<IAddPreloaderPayload>) {
		const { payload } = action;
		const list = new Set(Array.from(state.list));
		list.add(payload.preloaderName);
		return { ...state, list }
	},

	[preloader.REMOVE](state: IStore, action: IAction<IRemovePreloaderPayload>) {
		const { payload } = action;
		const list = new Set(Array.from(state.list));
		list.delete(payload.preloaderName);
		return { ...state, list }
	},
});
