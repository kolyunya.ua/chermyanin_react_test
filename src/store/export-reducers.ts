export { reducer as app } from './app';
export { reducer as auth } from './auth';
export { reducer as contacts } from './contacts';
export { reducer as modal } from './modal';
export { reducer as preloader } from './preloader';
export { reducer as profile } from './profile';
