import { combineReducers } from 'redux';
import { History } from 'history';
import * as reducers from './export-reducers';
import { connectRouter } from 'connected-react-router';

const createRootReducer = (history: History) => {
	const combinedReducers = combineReducers({
		router: connectRouter(history),
		...reducers
	});

	return (state: any, action: any) => {
		return combinedReducers(state, action);
	}
};

export { createRootReducer };
