import { createSelector } from 'reselect';
import { IStoreState } from '../types';
import { IStore, IDevice, IMenu } from './types';

const _getAppState = (state: IStoreState): IStore => state.app;
const _getState = (state: IStoreState): IStoreState => state;

export const getState = createSelector(
	[_getState],
	(state): IStoreState => state,
);

export const getApp = createSelector(
	[_getAppState],
	(app): IStore => app,
);

export const getDevice = createSelector(
	[getApp],
	(app): IDevice => app.device,
);

export const getMenu = createSelector(
	[getApp],
	(app): IMenu => app.menu,
);

export const getMobileNavVisible = createSelector(
	[getApp],
	(app): boolean => app.mobileNav.visible,
);