import { ValuesOfObject } from 'definitions/common-types';



export interface IDevice {
	isDesktop: boolean;
	isMobile: boolean;
}

export interface ISidebar {
	collapsed: boolean;
}

export interface IMenu {
	openKeys: string[];
}

export interface IMobileNav {
	visible: boolean;
}

export interface IStore {
	device: IDevice;
	menu: IMenu;
	mobileNav: IMobileNav;
}
