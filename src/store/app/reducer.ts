import BrowserizrNS from 'browserizr';
import { createReducer } from '../utils';
import { IAction } from '../types';
import { IStore, IMenu } from './types';
import { ETypes as app } from './actions';

const browser = BrowserizrNS.detect();

export const initialState: IStore = {
	device: {
		isDesktop: browser.isDesktop(),
		isMobile: browser.isMobile(),
	},
	menu: {
		openKeys: [],
	},
	mobileNav: {
		visible: false,
	}
};

export const reducer = createReducer<IStore>(initialState, {

	[app.MENU_ITEM_OPEN_CHANGE](state: IStore, action: IAction<IMenu>) {
		const { payload } = action;
		return {
			...state,
			menu: {
				...state.menu,
				openKeys: [payload.openKeys.pop()],
			},
		};
	},

	[app.MOBILE_NAV_TOGGLE](state: IStore) {
		return {
			...state,
			mobileNav: {
				...state.mobileNav,
				visible: !state.mobileNav.visible,
			},
		};
	}
});
