import { createAction } from '../utils';
import { IMenu } from './types';

export enum ETypes {
	MENU_ITEM_OPEN_CHANGE 				= '@app/MENU_ITEM_OPEN_CHANGE',
	MOBILE_NAV_TOGGLE 					= '@app/MOBILE_NAV_TOGGLE',
}


export const menuItemOpenChange 		= createAction<IMenu>(ETypes.MENU_ITEM_OPEN_CHANGE);
export const mobileNavToggle 			= createAction(ETypes.MOBILE_NAV_TOGGLE);
