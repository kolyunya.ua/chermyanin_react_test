import { takeEvery } from 'redux-saga/effects';
import { IStore as IProfile } from 'store/profile/types';
import { ETypes as profile } from 'store/profile/actions';
import { IAction } from '../types';

function* profileSaveListener (action: IAction<IProfile>) {
}

export function* saga() {
	yield takeEvery([profile.SAVE_PROFILE], profileSaveListener);
}
