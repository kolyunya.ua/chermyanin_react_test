import { PlainObject } from '../definitions/common-types';
import { IAction } from '../store/types';

/**
 * Creates an actionCreator function which accepts `payload` and a `type` from lexical scope
 * @param {string} type
 * @returns {function} actionCreator
 */
export function createAction<P = PlainObject>(type: string) {
	const actionCreator = (payload: P = {} as P): IAction<P> => ({type, payload});
	actionCreator.type = type;
	return actionCreator;
}

/**
 * Creates a standardized reducer with "type" and "payload" keys
 * @param initialState
 * @param reducerMap
 * @returns {function(*=, *)}
 */
export function createReducer<S>(initialState: S, reducerMap: PlainObject) {
	return (state: S = initialState, action: IAction) => {
		const reducer = reducerMap[action.type];

		return reducer
			? reducer(state, action)
			: state;
	};
}
