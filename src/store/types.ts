import { RouterState as IRouterState } from "connected-react-router";
import { PlainObject } from "../definitions/common-types";
import { IStore as IApp } from "./app/types";
import { IStore as IContacts } from "./contacts/types";
import { IStore as IModal } from "./modal/types";
import { IStore as IPreloader } from "./preloader/types";
import { IStore as IProfile } from "./profile/types";
import { IStore as IAuth } from "./auth/types";

export interface IStoreState {
	router: IRouterState;
	app: IApp;
	auth: IAuth;
	contacts: IContacts;
	modal: IModal;
	preloader: IPreloader;
	profile: IProfile;
}

export interface IAction<P = PlainObject> {
	type: string;
	payload: P;
}

export interface IQueryParams {
	params: PlainObject;
}

export interface IIncomingCollection<D = PlainObject> {
	info?: {
		page?: number;
		results?: number;
		seed?: string;
		version?: string;
	};
	results: D[];
}

export interface IFilters {
	page?: number;
	per_page?: number;
	order_by?: string;
	order_type?: "asc" | "desc";
}
