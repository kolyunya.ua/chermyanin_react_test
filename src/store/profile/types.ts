import { ID, PlainObject } from "definitions/common-types";
import { IIncomingCollection } from "store/types";

export interface IProfilePicture {
	large: string;
	medium: string;
	thumbnail: string;
}

export interface IProfileAge {
	age: number;
	date: string;
}

export interface IProfileName {
	first: string;
	last: string;
	title: string;
}

export interface IProfilePicture {
	large: string;
	medium: string;
	thumbnail: string;
}

export interface IProfileLocation {
	city: string;
	country: string;
	postcode: number;
	state: string;
	street: {
		name: string;
		number: number;
	};
}

export interface IIncomingProfile {
	dob: IProfileAge;
	email: string;
	gender: string;
	login: {
		uuid: string;
	};
	name: IProfileName;
	nat: string;
	phone: string;
	picture: IProfilePicture;
	location: IProfileLocation;
}

export interface IProfile {
	age: number | null;
	date: string;
	full_name: string;
	address: string;
	country: string;
	email: string;
	gender: string;
	id: ID;
	nat: string;
	phone: string;
	picture: IProfilePicture;
}

export interface IProfileList extends IIncomingCollection<IIncomingProfile> {}

export interface IStore {
	records: {
		list: PlainObject<IProfile>;
	};
}
