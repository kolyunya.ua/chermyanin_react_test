import { createSelector } from 'reselect';
import { IStoreState } from '../types';
import { IStore as IProfile } from './types';

const _getProfile = (state: IStoreState): IProfile => state.profile;

export const getProfile = createSelector(
	[_getProfile],
	(profile): any => profile.records.list

);