import { createReducer } from "../utils";
import { IAction } from "../types";
import {IStore } from "./types";
import { ETypes as profile } from "./actions";
export const initialState: IStore = {
	records: {
		list: {},
	},
};

export const reducer = createReducer<IStore>(initialState, {
	[profile.SAVE_PROFILE](state: IStore, action: IAction<any>) {
		const { payload } = action;
		return {
			...state,
			records: {
				...state.records,
				list: {
					...state.records.list,
					...payload.results.results[0],
				}
			}
		};
	},
});
