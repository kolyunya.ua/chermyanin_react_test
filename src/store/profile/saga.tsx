import { call, put, takeEvery, select } from 'redux-saga/effects';
import { api, Storage } from 'services';
import { IAction } from '../types';
import { workerMiddleware } from '../worker-middleware';
import { ETypes as auth } from '../auth/actions';
import {
	ETypes as profile,
	fetchProfile,
	saveProfile,
} from './actions';
import { getAuth } from 'store/auth/selectors';


function* authLoginListener() {
	const { seed }: any = yield select(getAuth);
	let requestPayload = null;
	if (seed) {
		requestPayload = seed
	}
	else {
		requestPayload = Storage.get('seed')
	}
	yield put(fetchProfile({ params: { seed: requestPayload } }));
}

function* fetchProfileWorker(action: IAction<any>) {
	const { payload = {} } = action;
	try {
		const response: any = yield call(api.profile.fetchProfile, payload);
		yield put(saveProfile(response));
	} catch (error) {
		throw error;
	}
}

export function* saga() {
	yield takeEvery(auth.LOGIN_SUCCESS, authLoginListener);
	yield takeEvery(profile.FETCH_PROFILE, workerMiddleware, { worker: fetchProfileWorker });
}
