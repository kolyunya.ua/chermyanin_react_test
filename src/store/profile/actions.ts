import { createAction } from '../utils';

export enum ETypes {
	FETCH_PROFILE 						= '@profile/FETCH_PROFILE',
	SAVE_PROFILE 						= '@profile/SAVE_PROFILE',
}

export const fetchProfile 		= createAction(ETypes.FETCH_PROFILE);
export const saveProfile 		= createAction(ETypes.SAVE_PROFILE);
