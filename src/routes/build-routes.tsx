import React from 'react';
import { Route } from 'react-router-dom';
import { ELayoutTypes } from '../constants/layouts';
import { Base as BaseLayout } from '../layouts/base';
import { IRoutesTree, TPageNames } from './types';

const buildRoutes = (routes: IRoutesTree) => {
	return Object.keys(routes).map((key) => {
		const { layout, page: Page, ...rest } = routes[key as TPageNames];

		switch (layout) {
			case ELayoutTypes.BASE:
				return (
					<Route key={rest.path} {...rest} render={(matchProps) => (
						<BaseLayout>
							<Page {...matchProps} />
						</BaseLayout>
					)} />
				);
			default:
				return (
					<Route key={rest.path} {...rest} render={(matchProps) => (
						<BaseLayout>
							<Page {...matchProps} />
						</BaseLayout>
					)} />
				);
		}
	});

};

export { buildRoutes };
