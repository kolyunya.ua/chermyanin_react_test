import { IRoutesTree } from "./types";
import { pages } from "./pages";
import { ELayoutTypes } from "../constants/layouts";
const routes: IRoutesTree = {
	logout: {
		path: `/logout`,
		layout: ELayoutTypes.BASE,
		page: pages["logout"],

		get name() {
			return "Logout";
		},
		link() {
			return this.path;
		},
	},
	home: {
		path: `/`,
		page: pages["home"],
		layout: ELayoutTypes.BASE,

		get name() {
			return "Home";
		},
		link() {
			return this.path;
		},

		exact: true,
	},
	profile: {
		path: `/profile`,
		page: pages["profile"],
		layout: ELayoutTypes.BASE,

		get name() {
			return "Profile";
		},

		link() {
			return this.path;
		},
	},

	contacts: {
		path: `/contacts`,
		page: pages["contacts"],
		layout: ELayoutTypes.BASE,

		get name() {
			return "Contacts";
		},
		link() {
			return this.path;
		},
		exact: true,
	},
	"contacts-view": {
		path: `/contacts/:id([0-9]+)`,
		layout: ELayoutTypes.BASE,
		page: pages["contacts-view"],

		get name() {
			return "Contacts viewing";
		},
		link(id) {
			return `/contacts/${id}`;
		},
		exact: true,
	},
	"page-not-found": {
		path: `*`,
		page: pages["page-not-found"],
		layout: ELayoutTypes.BASE,

		get name() {
			return "Page not found";
		},
		link() {
			return `/page-not-found`;
		},
	},
};

const __ROOT_ROUTE__ = routes.home.link();

export { routes, __ROOT_ROUTE__ };
