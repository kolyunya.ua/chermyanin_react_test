import { RoutesSwitch } from './routes-switch';
import { routes, __ROOT_ROUTE__ } from './routes-tree';
import { loadable } from './loadable';

export { routes, __ROOT_ROUTE__, loadable, RoutesSwitch };
