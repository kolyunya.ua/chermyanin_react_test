import React from 'react';
import { RouteProps, match } from 'react-router';
import { ELayoutTypes } from '../constants/layouts';
import { IStoreState } from '../store/types';

/**
 * Common
 * */
export interface IRouteMatch<RouteMatchParams = {}> extends match<RouteMatchParams> {}
export type MakeBreadcrumbsFn<RouteMatchParams> = (state: IStoreState, routeMatch: IRouteMatch<RouteMatchParams>) => React.ReactNode;
export type Breadcrumb = React.ReactNode | null;
export interface IRoute extends RouteProps {
	path: string;
	page: any;
	layout: ELayoutTypes;
	name: string;
	link(...args: any[]): string;
	exact?: boolean;
}



/**
 * IOwnProps
 * */
export interface IOwnProps<RouteMatchParams> {
	match: IRouteMatch<RouteMatchParams>;
}



/**
 * TConnectProps
 * */
export interface IMapStateToProps {
	state: IStoreState;
}
export interface IMapDispatchToProps {}
export type TConnectProps = IMapStateToProps & IMapDispatchToProps;



/**
 * TComponentProps
 * */
export type TComponentProps<RouteMatchParams> = IOwnProps<RouteMatchParams> & TConnectProps;



/**
 * Routes and Pages
 * */
export type TPageNames =
	| 'logout'
	| 'home'
	| 'profile'
	| 'contacts'
	| 'contacts-view'
	| 'page-not-found';
export type IRoutesTree = Record<TPageNames, IRoute>;
export type IPagesTree = Record<TPageNames, React.ReactNode>;
