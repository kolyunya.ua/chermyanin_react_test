import React from 'react';
import { Spin } from 'antd';

const rootStyles = {
	display: 'flex',
	justifyContent: 'center',
	alignItems: 'center',
	width: '100%',
	height: '100%',
};

export default <div style={rootStyles}><Spin size={'large'} /></div>;
