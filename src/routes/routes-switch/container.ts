import { compose } from 'redux';
import { connect } from 'react-redux';
import { IStoreState } from '../../store/types';
import { getProfile } from '../../store/profile/selectors';
import { IMapStateToProps, IMapDispatchToProps } from './types';
import { View } from './view';

const mapStateToProps = (state: IStoreState): IMapStateToProps => {
	return {
		uniqueKey: String(getProfile(state).id),
	};
};

const mapDispatchToProps: IMapDispatchToProps = {};

const RoutesSwitch = compose(
	connect(mapStateToProps, mapDispatchToProps),
)(View);

export { RoutesSwitch };
