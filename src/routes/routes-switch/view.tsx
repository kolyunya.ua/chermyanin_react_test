import React from 'react';
import { Switch } from 'react-router-dom';
import { buildRoutes } from '../build-routes';
import { routes } from '../routes-tree';
import { TComponentProps } from './types';

const View: React.FC<TComponentProps> = (props) => {
	const { uniqueKey } = props;

	return (
		<Switch key={uniqueKey}>{buildRoutes(routes)}</Switch>
	);
};

export { View };
