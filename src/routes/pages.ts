import LoadablePreloader from "./loadable-preloader";
import { loadable } from "./loadable";
import { isAuth } from "./auth-middleware";
import { IPagesTree } from "./types";

const pages: IPagesTree = {
	'logout': isAuth(
		loadable(
			"pages/logout",
			() => import("../pages/logout"),
			() => LoadablePreloader
		)
	),
	'home': 
		loadable(
			"pages/home",
			() => import("../pages/home"),
			() => LoadablePreloader
	),

	profile: isAuth(
		loadable(
			"pages/profile",
			() => import("../pages/profile"),
			() => LoadablePreloader
		)
	),
	contacts: isAuth(
		loadable(
			"pages/contacts",
			() => import("../pages/contacts"),
			() => LoadablePreloader
		)
	),
	"contacts-view": isAuth(
		loadable(
			"pages/conacts-view",
			() => import("pages/contacts-view"),
			() => LoadablePreloader
		)
	),
	"page-not-found": loadable(
		"pages/not-found",
		() => import("../pages/page-not-found"),
		() => LoadablePreloader
	),
};

export { pages };
