import React from 'react';
import { connectedRouterRedirect } from 'redux-auth-wrapper/history4/redirect';
import { Preloader } from 'components';
import { IStoreState } from 'store/types';
import { routes } from 'routes/routes-tree';

export const isAuth = connectedRouterRedirect({
    redirectPath: () => routes['profile'].link(),
    authenticatedSelector: (state: IStoreState): boolean => !!(state.auth.isAuthenticated),
    AuthenticatingComponent: () => <Preloader hideChildren={true} />,
    wrapperDisplayName: 'UserIsAuthenticated',
});
