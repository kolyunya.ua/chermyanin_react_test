import Validator from 'validatorjs';
import { AxiosResponse } from 'axios';
import { PlainObject } from 'definitions/common-types';

interface IValidationGuardErrorConstructor {
	reason: string;
	status: number;
	input: PlainObject;
	rules: PlainObject;
	errors: PlainObject;
}
export class ValidationGuardError extends Error {
	public readonly status: number = NaN;
	public readonly input: PlainObject;
	public readonly rules: PlainObject;
	public readonly errors: PlainObject;

	constructor(params: IValidationGuardErrorConstructor) {
		super();

		this.name = 'ValidationGuardError';
		this.message = params.reason;

		this.status = params.status;
		this.input = params.input;
		this.rules = params.rules;
		this.errors = params.errors;
	}
}


export interface DataGuardConstructor {
	new (response: AxiosResponse): PlainObject;
}
export enum EGuardTypes {
	SERVER = 'SERVER',
}

interface IBaseGuardConstructor {
	guardType: EGuardTypes;
}
export class BaseGuard {
	private readonly serverGuardTypeRegExp = new RegExp(`^${EGuardTypes.SERVER}$`);

	protected readonly guardRules = {};
	protected guardType: EGuardTypes;
	protected requestResponse: AxiosResponse = {} as AxiosResponse;

	constructor ({ guardType }: IBaseGuardConstructor) {
		this.guardType = guardType;
	}

	protected validate<T extends {} = {}>(payload: T) {
		const validation = new Validator(payload, this.guardRules);

		if (validation.fails()) {
			let reason = 'Unknown reason';
			if (this.serverGuardTypeRegExp.test(this.guardType)) {
				reason = 'Incoming data from the server to the client is not valid';
			}
			this.handleError(new ValidationGuardError({
				reason,
				status: NaN,
				input: validation.input,
				rules: validation.rules,
				errors: validation.errors.all(),
			}));
		}
		return validation;
	}

	private handleError (error: ValidationGuardError): void {
		const { name, message, input, rules, errors } = error;
		console.error(
			[name, message].join(': '), '\n',
			'Validation input => ', input, '\n',
			'Validation rules => ', rules, '\n',
			'Validation errors => ', errors, '\n',
		);
		if (this.serverGuardTypeRegExp.test(this.guardType)) {
			const { status = NaN, statusText = '', config = {}, data = {} } = this.requestResponse;
			console.error(
				'Status => ', status, '\n',
				'Status text => ', statusText, '\n',
				'Request config => ', config, '\n',
				'Response data => ', data, '\n',
			);
		}

		throw error;
	}
}
