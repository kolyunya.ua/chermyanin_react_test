import { IProfileList } from 'store/profile/types';
import { http } from '../../http';
import { endpoint } from '../endpoints';
import { IProfileApi } from './types';
import { ProfileGuard } from './guards';

export const profile: IProfileApi = {
	fetchProfile: (payload) => {
		return http.get<IProfileList>(endpoint.contacts.FETCH_CONTACTS, { params: payload?.params })
			.then((response): ProfileGuard => new ProfileGuard(response));
	},
};
