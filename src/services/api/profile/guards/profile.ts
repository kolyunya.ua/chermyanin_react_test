import { makeAddress } from '../../../../utils/common';
import dayjs from "dayjs";
import { AxiosResponse } from "axios";
import { IContactsCollectionList } from "store/contacts/types";
import { makeFullName, removeProps } from 'utils';
import { BaseGuard, EGuardTypes } from "../../base-guard";
import { EDateTime } from "constants/date-time";

interface __IData {
	results: IContactsCollectionList;
}

export class ProfileGuard extends BaseGuard implements __IData {
	public readonly results: IContactsCollectionList = {} as IContactsCollectionList;

	protected readonly guardRules = {
		results: "array",
		"results.*.dob.age": "numeric",
		"results.*.dob.date": "string",
		"results.*.email": "string",
		"results.*.gender": "string",
		"results.*.login.uuid": "string",
		"results.*.name.first": "string",
		"results.*.name.last": "string",
		"results.*.name.title": "string",
		"results.*.nat": "string",
		"results.*.phone": "string",
		"results.*.picture.large": "string",
		"results.*.picture.medium": "string",
		"results.*.picture.thumbnail": "string",
		"results.*.location.city": "string",
		"results.*.location.country": "string",
		"results.*.location.postcode": "numeric",
		"results.*.location.state": "string",
		"results.*.location.street.name": "string",
		"results.*.location.street.number": "numeric",
	};

	constructor(response: AxiosResponse<IContactsCollectionList>) {
		super({
			guardType: EGuardTypes.SERVER,
		});
		const { data: payload } = response;
		payload.results = payload.results.map((record) => {
			const { date, age } = record.dob;
			const { title, first, last } = record.name;
			const { city, country, postcode, state, street  } = record.location;
			const { name, number } = street;
			const { uuid } = record.login;
			const full_name = makeFullName(title, first, last);
			const address = makeAddress(number, name, city, state, postcode);
			const formattedDate = dayjs(
				typeof date === "number" ? date * 1000 : date
			).format(EDateTime.CLIENT_DATE_TIME_FORMAT);
			record = removeProps(record, ['dob', 'registered', 'id', 'cell', 'name', 'location', 'login']);
			return {
				...record,
				date: formattedDate,
				age,
				full_name,
				address,
				country,
				id: uuid
			};
		});

		this.requestResponse = response;
		this.validate<IContactsCollectionList>(payload);

		this.results = payload;
	}
}
