import { ProfileGuard } from './guards/profile';
import { IQueryParams } from 'store/types';

export interface IProfileApi {
	fetchProfile: (payload?: IQueryParams) => Promise<ProfileGuard>;
}
