import { IContactsCollectionList } from 'store/contacts/types';
import { http } from '../../http';
import { endpoint } from '../endpoints';
import { IContactsApi } from './types';
import { ContactsCollectionGuard } from './guards';

export const contacts: IContactsApi = {
	fetchContacts: (payload) => {
		return http.get<IContactsCollectionList>(endpoint.contacts.FETCH_CONTACTS, { params: payload?.params })
			.then((response): ContactsCollectionGuard => new ContactsCollectionGuard(response));
	}
};
