import { IQueryParams } from 'store/types';
import { ContactsCollectionGuard } from './guards';

export interface IContactsApi {
	fetchContacts: (payload?: IQueryParams) => Promise<ContactsCollectionGuard>;
}
