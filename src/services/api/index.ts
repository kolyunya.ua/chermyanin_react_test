import { ALLOW_WINDOW_EXTENDS } from 'constants/env';
import { IApi } from './types';
import { profile } from './profile';
import { contacts } from './contacts';

const api: IApi = {
	profile,
	contacts
};

if (ALLOW_WINDOW_EXTENDS) {
	window.$api = api;
}

export { api };
