import { PlainObject } from 'definitions/common-types';
import { IContactsApi } from './contacts/types';
import { IProfileApi } from './profile/types';

export interface IApi extends PlainObject {
	contacts: IContactsApi;
	profile: IProfileApi;
}
