import axios, { AxiosInstance } from 'axios';
import { API_URL, ALLOW_WINDOW_EXTENDS, API_VERSION } from 'constants/env';

const http: AxiosInstance = axios.create({
	baseURL: `${API_URL}/${API_VERSION}`,
	headers: {
		'Content-Type': 'application/json',
		'Accept': 'application/json',
	}
});

if (ALLOW_WINDOW_EXTENDS) {
	window.$http = http;
}

export { http };
