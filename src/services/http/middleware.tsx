import React from 'react';
import { AxiosError, AxiosResponse } from 'axios';
import { List } from 'antd';
import { routes } from '../../routes';
import { redirect } from 'utils';
import { Notify } from './../index';

export const middleware: {
	handleError(error: AxiosError): void;
} = {
	handleError: (error) => {
		const { response } = error as AxiosError;
		if (response) {
			const { status, statusText, config, data } = response as AxiosResponse;
			const { url } = config;
			console.error(
				'Status => ', status, '\n',
				'Status text => ', statusText, '\n',
				'Request config => ', config, '\n',
				'Response data => ', data, '\n',
			);
			const showErrors = () => {
				const { errors } = data;
				if (errors.length === 1) {
					Notify.create({
						type: 'error',
						message: errors[0].title,
					});
				} else {
					Notify.create({
						type: 'error',
						message: <strong>{'Errors that need to be fixed'}</strong>,
						description: (
							<List
								size={'small'}
								dataSource={errors}
								renderItem={(error: { title: string; }, idx) => (
									<List.Item key={idx}>{error.title}</List.Item>
								)}
							/>
						)
					});
				}
			};

			switch (status) {
				case 400: // Кривой запрос
					Notify.error('Something went wrong', 'Details can be found in the developer console');
					break;

				case 403:
					// redirect(routes['forbidden'].link());
					Notify.error('You do not have enough rights');
					break;

				case 404:
					break;

				case 406:
					if (data.errors && data.errors.length) {
						showErrors();
					}
					break;

				case 500:
					Notify.error('Server error', 'Please, try again later');
					break;

				default:
					Notify.error('Something went wrong', 'Please, try again later');
					console.warn(`${status} ${statusText}`);
					break;
			}
		}
	}
};
