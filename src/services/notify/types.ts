import { ArgsProps as INotificationArgsProps } from 'antd/lib/notification/index'
import { AntdStatusTypes } from 'definitions/common-types';

export type NotifyStatusTypes = AntdStatusTypes | 'dev';

export interface IDefaults extends Partial<INotificationArgsProps> {}

export interface IParams extends INotificationArgsProps {}
