import React from 'react';
import BrowserizrNS from 'browserizr';
import { notification } from 'antd';
// import Icon from 'antd';
import { ALLOW_WINDOW_EXTENDS } from '../../constants/env';
import { IDefaults, IParams, NotifyStatusTypes } from './types';

const browser = BrowserizrNS.detect();

class Notify {
	public static create(params: IParams): void {
		const defaults: IDefaults = {
			top: browser.isMobile() ? 10 : 86,
			bottom: 10,
			placement: 'topRight',
		};
		const config: IParams = { ...defaults, ...params };

		notification.open(config);
	}

	public static dev(): void {
		this.create({
			type: 'info',
			message: <b>Work in progress!</b>,
			// icon: <Icon type={'tool'} />,
		});
	}

	public static info(message: string, description?: string): void {
		this.create({ type: 'info', message, description });
	}

	public static success(message: string, description?: string): void {
		this.create({ type: 'success', message, description });
	}

	public static warning(message: string, description?: string): void {
		this.create({ type: 'warning', message, description });
	}

	public static error(message: string, description?: string): void {
		this.create({ type: 'error', message, description });
	}

	public static echo(): void {
		const statuses: NotifyStatusTypes[] = ['dev', 'info', 'success', 'warning', 'error'];
		statuses.forEach((status) => window.setTimeout(() => Notify[status](status), 0));
	}
}

if (ALLOW_WINDOW_EXTENDS) {
	window.$notify = Notify;
}

export { Notify };
