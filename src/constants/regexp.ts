export const MATCH_ID_IN_URL_REGEXP = /\/([0-9]+)$/;
export const LEAVE_NUMBERS_POINT_AND_COMMA = /[^\d.,]/ig;
export const LEAVE_NUMBERS_ONLY = /[^0-9]/ig;
export const LEAVE_ONLY_VALID_CSS_CHARACTERS = /[^0-9\w\-_]/gi;
export const MATCH_FIELD_INDEX = /\[([0-9]+)\]/;
export const ALPHA_NUMERIC = /[^A-Z0-9]/ig;
