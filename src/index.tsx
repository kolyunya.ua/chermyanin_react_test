import React from 'react';
import ReactDOM from 'react-dom';
import { Provider as ReduxProvider } from 'react-redux';
import { ConnectedRouter as ConnectedRouterProvider } from 'connected-react-router';
import { Root } from 'components';
import { history } from 'routes/history';
import { restoreAuth, store, } from 'store';

const root = document.getElementById('root')! as HTMLElement;
const render = (Component: React.ComponentType, root: HTMLElement, done?: () => void) => {
  ReactDOM.render(
    <ReduxProvider store={store}>
      <ConnectedRouterProvider history={history}>
        <Component />
      </ConnectedRouterProvider>
    </ReduxProvider>,
    root,
    done,
  );
};
restoreAuth();
render(Root, root);