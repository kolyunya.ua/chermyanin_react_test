import React from 'react';

export type ID = string | number;

export type AntdStatusTypes = 'success' | 'info' | 'error' | 'warning';
export type AntdPlacementTypes = 'top' | 'right' | 'bottom' | 'left';

export type PlainObject<Value = any> = {
	[p: string]: Value;
};

export type KeysOfObject<T extends Object> = keyof T;
export type ValuesOfObject<T extends Object> = T[KeysOfObject<T>];

export interface IdName {
	id: ID;
	name: string;
}

export interface IStyles extends PlainObject<React.CSSProperties> {}
