export type TValidationErrorObject = {
	[n: string]: {
		[s: string]: string
	}
}

export interface IServerValidationError {
	source: {
		parameter: string;
	},
	title: string;
	status: number;
}
