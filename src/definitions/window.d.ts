
import { AxiosInstance } from 'axios';
import { Store } from 'redux';
import { Notify } from 'services/notify';
import { Storage } from 'services/storage';
import { IApi } from 'services/api/types';
import { IStoreState } from 'store/types';

/*
|-----------------------------------------------------------------------------------------------------------------------
| https://dev.to/cogoo/how-to-set-a-new-property-on-the-window-object-in-typescript-3jeh
|-----------------------------------------------------------------------------------------------------------------------
*/
export declare global {
	interface Window {
		// Your custom global property
		__INITIAL_STATE__: IStoreState;
		$api: IApi;
		$http: AxiosInstance;
		$store: Store;
		$notify: Notify;
		$storage: Storage;
	}
}
