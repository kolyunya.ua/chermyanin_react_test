/*eslint-disable react-hooks/exhaustive-deps*/
import { PageHeading, TableContacts } from 'components';
import { ContactsFilter } from 'components';
import React, { Fragment } from 'react';
import Helmet from 'react-helmet';
import { TComponentProps } from './types';

const View: React.FC<TComponentProps> = () => {
	return (
		<Fragment>
			<Helmet title={'Contacts'} />
			<div className={'page page--contacts'}>
			<div className={'page__heading'}>
					<PageHeading title={'Contacts'} />
				</div>
				<div className={'page__body'}>
					<ContactsFilter />
					<TableContacts />
				</div>
			</div>
		</Fragment>
	)
};

export { View };
