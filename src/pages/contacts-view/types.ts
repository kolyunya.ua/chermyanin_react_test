import { RouteComponentProps } from 'react-router';
import { IAction } from 'store/types';
import { IFetchContactsRequestPayload } from 'store/contacts/types';

/**
 * IOwnProps
 * */
export interface IOwnProps {}



/**
 * TConnectProps
 * */
export interface IMapStateToProps {}
export interface IMapDispatchToProps {
	fetchContact(payload: IFetchContactsRequestPayload): IAction<IFetchContactsRequestPayload>;
}
export type TConnectProps = IMapStateToProps & IMapDispatchToProps;



/**
 * IWithRouterProps
 * */
export interface IWithRouterProps extends RouteComponentProps<{ id: string }> {}



/**
 * TComponentProps
 * */
export type TComponentProps = IOwnProps & TConnectProps & IWithRouterProps;

