import { compose } from 'redux';
import { connect } from 'react-redux';
import { IStoreState } from 'store/types';
import { fetchContact } from 'store/contacts/actions';
import { IMapStateToProps, IMapDispatchToProps } from './types';
import { View } from './view';

const mapStateToProps = (state: IStoreState): IMapStateToProps => {
	return {};
};

const mapDispatchToProps: IMapDispatchToProps = { fetchContact };

const PageContactsEdit = compose(
	connect(mapStateToProps, mapDispatchToProps),
)(View);

export { PageContactsEdit };
