import React, { FC, useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { PageHeading, Box } from 'components';
import { routes } from 'routes';
import { TComponentProps } from './types';

const View: FC<TComponentProps> = (props) => {
	const { match, fetchContact } = props;
	const { id: contactId } = match.params;

	useEffect(() => {
		if (contactId) {
			// fetchContact({ });
		}
	}, [contactId, fetchContact]);

	return (
		<>
			<Helmet title={routes['contacts-view'].name} />
			<div className={'page page--contacts-edit'}>
				<div className={'page__heading'}>
					<PageHeading title={routes['contacts-view'].name} />
				</div>
				<div className={'page__body'}>
					<Box>
					</Box>
				</div>
			</div>
		</>
	)
};

export { View };
