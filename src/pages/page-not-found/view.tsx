/*eslint-disable react-hooks/exhaustive-deps*/
import { Button } from 'antd';
import React, { Fragment } from 'react';
import Helmet from 'react-helmet';
import { routes } from 'routes';
import { redirect } from 'utils';
import { TComponentProps } from './types';

const View: React.FC<TComponentProps> = () => {
	return (
		<Fragment>
		<Helmet title={'Requested page not found!'} />
		<div className={'page page--not-found'}>
			<div className={'page__body'}>
				<div className={'error error--404'}>
					<div className={'error__code'}>404</div>
					<div className={'error__message'}>{'Requested page not found!'}</div>
					<div className={'error__back-button'}>
						<Button
							className={'button button--brand'}
							type={'primary'}
							htmlType={'button'}
							size={'large'}
							onClick={() => redirect(routes['home'].link())}
						>
							{'Back home'}
						</Button>
					</div>
				</div>
			</div>
		</div>
	</Fragment>
	)
};

export { View };
