/*eslint-disable react-hooks/exhaustive-deps*/
import { Col, Divider, Row, Tag, Typography } from 'antd';
import Avatar from 'antd/lib/avatar/avatar';
import { PageHeading, Truncate } from 'components';
import React, { Fragment } from 'react';
import Helmet from 'react-helmet';
import { TComponentProps } from './types';

const View: React.FC<TComponentProps> = (props) => {
	const { profile } = props;
	const { picture, full_name, age, email, phone, address, country, nat } = profile;
	const { large } = picture;
	return (
		<Fragment>
			<Helmet title={'Profile'} />
			<div className={'page page--profile'}>
				<div className={'page__heading'}>
					<PageHeading title={'Profile'} />
				</div>
				<div className={'page__body'}>
					<Row justify={'center'} gutter={[20, 20]}>
						<Col>
							<Avatar src={large} shape="square" size={260}></Avatar>
						</Col>
						<Col>
							<Typography.Title level={3}>{full_name} <Typography.Text type={'secondary'}>({age} years)</Typography.Text></Typography.Title>
							<Divider />
							<Truncate type={'email'} copy={email}>{email}</Truncate>
							<Truncate.Phone phone={{ number: phone }} />
							<Truncate type={'text'} copy={`[${country}] ${address}`}><strong>/{country}/</strong> {address}</Truncate>
							<Divider />
							<Tag color={'blue'}>{nat}</Tag>
						</Col>

					</Row>
				</div>
			</div>
		</Fragment>
	)
};

export { View };
