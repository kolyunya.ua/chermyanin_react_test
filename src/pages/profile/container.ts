import { getProfile } from 'store/profile/selectors';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { IStoreState } from '../../store/types';
import { IMapStateToProps, IMapDispatchToProps } from './types';
import { View } from './view';

const mapStateToProps = (state: IStoreState): IMapStateToProps => {
	return {
		profile: getProfile(state)
	};
};

const mapDispatchToProps: IMapDispatchToProps = {};

const PageProfile = compose(
	connect(mapStateToProps, mapDispatchToProps)
)(View);

export { PageProfile };
