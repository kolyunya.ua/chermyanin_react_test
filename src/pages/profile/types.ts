import { IProfile } from './../../store/profile/types';
// import { IAction } from 'store/types';

/**
 * IOwnProps
 * */
export interface IOwnProps {}



/**
 * TConnectProps
 * */
export interface IMapStateToProps {
    profile: IProfile;
}
export interface IMapDispatchToProps {}
export type TConnectProps = IMapStateToProps & IMapDispatchToProps;



/**
 * TComponentProps
 * */
export type TComponentProps = IOwnProps & TConnectProps;
