import { compose } from 'redux';
import { connect } from 'react-redux';
import { IStoreState } from 'store/types';
import { logoutRequest } from 'store/auth/actions';
import { IMapStateToProps, IMapDispatchToProps } from './types';
import { View } from './view';

const mapStateToProps = (state: IStoreState): IMapStateToProps => {
	return {};
};

const mapDispatchToProps: IMapDispatchToProps = {
	logoutRequest,
};

const PageLogout = compose(
	connect(mapStateToProps, mapDispatchToProps)
)(View);

export { PageLogout };
