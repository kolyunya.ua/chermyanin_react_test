/*eslint-disable react-hooks/exhaustive-deps*/
import React, { Fragment, useEffect } from 'react';
import Helmet from 'react-helmet';
import { TComponentProps } from './types';

const View: React.FC<TComponentProps> = ({ logoutRequest }) => {
	useEffect(() => {
		logoutRequest();
	}, []);

	return (
		<Fragment>
			<Helmet title={'Logout from the system'} />
			<div className={'page page--logout'}>
				<div className={'page__body'}>
					<div>{'Logout'}...</div>
				</div>
			</div>
		</Fragment>
	)
};

export { View };
