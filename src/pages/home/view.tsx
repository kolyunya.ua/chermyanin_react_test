/*eslint-disable react-hooks/exhaustive-deps*/
import React, { Fragment } from 'react';
import Helmet from 'react-helmet';
import { TComponentProps } from './types';

const View: React.FC<TComponentProps> = () => {
	return (
		<Fragment>
			<Helmet title={'Home Page'} />
			<div className={'page page--home'}>
				<div className={'page__body'}>

				</div>
			</div>
		</Fragment>
	)
};

export { View };
