import React from 'react';
import { IDevice } from '../../store/app/types';

/**
 * IOwnProps
 * */
export interface IOwnProps {
	children: React.ReactElement;
}



/**
 * TConnectProps
 * */
export interface IMapStateToProps {
	device: IDevice;
}
export interface IMapDispatchToProps {}
export type TConnectProps = IMapStateToProps & IMapDispatchToProps;



/**
 * TComponentProps
 * */
export type TComponentProps = IOwnProps & TConnectProps;
